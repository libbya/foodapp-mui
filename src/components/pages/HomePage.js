import React, { Fragment } from 'react';
import {Container, Button, Typography} from '@material-ui/core';
import LocationCard from '../LocationCard';
import ProceduralSnackbar from '../ProceduralSnackbar';
import AddIcon from '@material-ui/icons/Add';

const HomePage = props => {
    const {mealToShow, setMeal, favorites, locations} = props;
    if (!mealToShow) {
        return <div style={{minHeight:'80vh', display:'flex', flexDirection:'column', justifyContent:'center'}}>
            <Container>
                <div style={{
                    display:'flex',
                    flexDirection:'column',
                    alignContent:'center',
                    justifyContent:'center'
                }}>
                    <Button
                        onClick={() => setMeal('Breakfast')}
                        variant="contained"
                        color="primary"
                    >
                        Breakfast
                    </Button>
                    <br/>
                    <Button
                        onClick={() => setMeal('Lunch')}
                        variant="contained"
                        color="primary"
                    >
                        Lunch
                    </Button>
                    <br/>
                    <Button
                        onClick={() => setMeal('Late Lunch')}
                        variant="contained"
                        color="primary"
                    >
                        Late Lunch
                    </Button>
                    <br/>
                    <Button
                        onClick={() => setMeal('Dinner')}
                        variant="contained"
                        color="primary"
                    >
                        Dinner
                    </Button>
                </div>
            </Container>
        </div>
    }
    if (!locations.filter(loc => loc.menu.some(meal => meal.Type === mealToShow && meal.Status === 'Open')).length) {
        console.log(mealToShow, locations.map(loc => loc.menu.map(meal => meal.Type + ' ' + meal.Status)))
        return (
            <Fragment>
                <div style={{ marginTop: '30vh' }}>
                    <Typography variant="h5" component="h5" style={{textAlign: 'center'}}>No dining facilities are open for the selected meal</Typography>
                    <br/>
                    <Typography variant="h6" component="h6" style={{textAlign: 'center'}}>Try changing your filters in Settings</Typography>
                </div>
                <div style={{height:80}}></div>
            </Fragment>
        )
    }
    return (
        <Fragment>
            <div style={{display:'flex', flexDirection:'row', flexWrap:"wrap", justifyContent:'center', alignItems:'flex-start', alignContent:'space-around'}}>
                {showMealCards(mealToShow, favorites, locations, props.editFavorite)}
            </div>
        <div style={{height:80}}></div>
        </Fragment>
    );
}

const showMealCards = (meal, favorites, locations, editFavorite) => {
    return <Fragment>
    {locations.map(loc => {
        return <LocationCard key={loc.Name} location={loc} favorites={favorites} mealToShow={meal} editFavorite={editFavorite}/>
    })}
    {(!favorites || favorites.length === 0) && <ProceduralSnackbar
        message={<span>You don't seem to have any favorites! Click the &nbsp;<AddIcon style={{position:'relative', top:5}} fontSize='small'/> button to add some.</span>}
        anchorOrigin={{vertical:'bottom', horizontal:'left'}}
        autoTimeout={10000}
    />}
    </Fragment>
}
export default HomePage;
