import React from 'react';
import { Drawer } from '@material-ui/core';
import { DatePicker } from '@material-ui/pickers';


const Calendar = props => {
  return (
    <Drawer anchor="bottom" open={props.open} onClose={props.onClose}>
        <DatePicker 
            onChange={props.onChange}
            value={props.value}
            variant='static'
            autoOk={true}
        />
    </Drawer>
  );
}

export default Calendar;
