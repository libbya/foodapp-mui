import React, { useState, Fragment } from 'react';
import {Container, Card, CardActions, CardContent, Typography, TextField, Button} from '@material-ui/core';

const FavoritesPage = props => {
    const [query, setQuery] = useState('');
    const getCardList = () => {
        const query_regex = new RegExp(`(^${query})|(\\s${query})`, 'ig');
        const filtered = (query && query !== '') ? props.favorites.filter(item => item[0].match(query_regex)) : props.favorites
        return filtered && filtered.map(item => <Card key={item[0]}>
            <CardContent>
                <div style={{display:'flex', justifyContent:'space-between'}}>
                    <Typography variant="h5" component="span">
                        {item[0]}
                    </Typography>
                    <Typography variant="h5" component="span">
                        {isNaN(item[1]) ? 'NaN' : item[1]}
                    </Typography>
                </div>
            </CardContent>
            <CardActions>
                <Button
                    size="small"
                    onClick={() => props.editFavorite(item[0], item[1] || 1)}
                >Edit</Button>
                <Button
                    size="small"
                    color="secondary"
                    onClick={() => props.removeFavorite(item[0])}
                >Remove</Button>
            </CardActions>
        </Card>)
    }
    return (
        <Fragment>
            <div style={{position:'fixed', width:'100%', height:60, backgroundColor:'#ddd', zIndex:1, top:0, left:0}}></div>
            <Container>
                <div style={{position:'fixed', width:'calc(100% - 48px)', maxWidth:'90%', zIndex:2}}>
                    <TextField 
                        value={query}
                        onChange={e => setQuery(e.target.value)}
                        label="Search"
                    />
                </div>
                <div style={{height:80, backgroundColor:'#ddd'}}></div>
                <div id="card-container">
                    {getCardList()}
                </div>
                <div style={{height:80}}></div>
            </Container>
        </Fragment>
    );
}

export default FavoritesPage;
