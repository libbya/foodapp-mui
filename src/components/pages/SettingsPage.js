import React, { Fragment } from 'react';
import {Container, Card, CardContent, Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel} from '@material-ui/core';

const FavoritesPage = ({hiddenTypes, toggleHiddenType, toggleAllHiddenTypes}) => {
    return (
        <Fragment>
            <div style={{position:'fixed', width:'100%', height:60, backgroundColor:'#ddd', zIndex:1, top:0, left:0}}></div>
            <Container>
                <div style={{height:80, backgroundColor:'#ddd'}}></div>
                <div id="card-container">
                    <Card>
                        <CardContent>
                            <div style={{display:'flex', justifyContent:'flex-start'}}>
                                    <FormControl component="fieldset">
                                        <FormLabel component="legend">Show facilities by type:</FormLabel>
                                        <FormControlLabel
                                            style={{marginTop: "10px"}}
                                            control={<Checkbox checked={Object.entries(hiddenTypes).some(([_, show]) => show)} indeterminate={Object.entries(hiddenTypes).some(([_, show]) => show) && Object.entries(hiddenTypes).some(([_, show]) => !show)} onChange={() => toggleAllHiddenTypes()} name="all" />}
                                            label="All"
                                        />
                                        <FormGroup>
                                        <FormControlLabel
                                            style={{marginLeft: "10px"}}
                                            control={<Checkbox checked={hiddenTypes["Dining Courts"]} onChange={() => toggleHiddenType("Dining Courts")} name="dining courts" />}
                                            label="Dining Courts"
                                        />
                                        <FormControlLabel
                                            style={{marginLeft: "10px"}}
                                            control={<Checkbox checked={hiddenTypes["Quick Bites"]} onChange={() => toggleHiddenType("Quick Bites")} name="quick bites" />}
                                            label="Quick Bites"
                                        />
                                        <FormControlLabel
                                            style={{marginLeft: "10px"}}
                                            control={<Checkbox checked={hiddenTypes["On-the-GO!"]} onChange={() => toggleHiddenType("On-the-GO!")} name="on the go" />}
                                            label="On-the-GO!"
                                        />
                                        </FormGroup>
                                    </FormControl>
                            </div>
                        </CardContent>
                    </Card>
                </div>
                <div style={{height:80}}></div>
            </Container>
        </Fragment>
    );
}

export default FavoritesPage;
