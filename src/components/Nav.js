import React from 'react';
import {AppBar, Toolbar, IconButton, Fab, Button} from '@material-ui/core';
import HomeIcon from '@material-ui/icons/Home';
import AddIcon from '@material-ui/icons/Add';
import EventIcon from '@material-ui/icons/Event';
import FavoriteIcon from '@material-ui/icons/Favorite';
import SettingsIcon from '@material-ui/icons/Settings';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';

const useStyles = makeStyles(theme => ({
    appBar: {
        top: 'auto',
        bottom: 0,
    },
    grow: {
        flexGrow: 1,
    },
    fabButton: {
        position: 'absolute',
        zIndex: 1,
        top: -30,
        left: 0,
        right: 0,
        margin: '0 auto',
    },
}));
  

const Nav = props => {
    const classes = useStyles();
    return (
        <AppBar position="fixed" color="primary" className={classes.appBar}>
            <Toolbar>
            <Link to="/">
                <IconButton edge="start" color="inherit" onClick={() => props.setMeal('')}>
                    <HomeIcon />
                </IconButton>
            </Link>
            <Button component="span" color="inherit" onClick={() => props.setCalendar(true)}>
                <EventIcon />
                &nbsp;&nbsp;{format(props.selectedDate, 'MMM d')}
            </Button>
            <Fab color="secondary" aria-label="add" className={classes.fabButton} onClick={() => props.setAddFavorite(true)}>
                <AddIcon />
            </Fab>
            <div className={classes.grow} />
            <Link to="/settings" style={{marginRight:20}}>
                <IconButton edge="end" color="inherit">
                    <SettingsIcon />
                </IconButton>
            </Link>
            <Link to="/favorites">
                <IconButton edge="end" color="inherit">
                    <FavoriteIcon />
                </IconButton>
            </Link>
            </Toolbar>
        </AppBar>
    );
}

export default Nav;
