import React, { useState } from 'react';
import { Drawer, Container, Slider, TextField, Button } from '@material-ui/core';

const AddFavorite = props => {
    const [textError, setTextError] = useState({
        error:false,
        helperText:''
    })

    const handleScoreChange = (e, newValue) => {
        props.setForm({
            nameText: props.nameText,
            score: newValue
        });
    }

    const handleTextChange = e => props.setForm({
        nameText: e.target.value,
        score: props.score
    })

    const handleSubmit = e => {
        e.preventDefault();
        //console.log("Add favorite", props.nameText, props.score)

        if(!props.nameText || props.nameText === '') {
            setTextError({
                error: true,
                helperText: 'Food name cannot be blank'
            })
            return;
        }
        props.addFavorite();
        props.setForm({
            nameText:'',
            score:1
        })
        props.onClose();
    }

    const maxScore = 10
    const minScore = 1
    return (
        <Drawer anchor="bottom" open={props.open} onClose={props.onClose}>
            <div style={{margin:20}}>
            <Container>
                <form>
                    <TextField 
                        value={props.nameText}
                        onChange={handleTextChange}
                        required
                        label="Food name"
                        {...textError}
                    />
                    <br/>
                    <Slider
                        value={props.score}
                        onChange={handleScoreChange}
                        max={maxScore}
                        min={minScore}
                        valueLabelDisplay='auto'/>
                    <Button
                        onClick={handleSubmit}
                        color="primary"
                    >
                        Add Favorite
                    </Button>
                </form>
            </Container>
            </div>
        </Drawer>
    );
}

export default AddFavorite;
