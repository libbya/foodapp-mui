import React, { useMemo, Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {parse, format} from 'date-fns';

const useStyles = makeStyles(theme => ({
  card: {
    width:360,
    maxWidth: '100%',
    marginLeft:5,
    marginRight:5
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  }
}));

/*
 * Menu
 *   Stations[]
 *     Items[]
 *       Name
 */

export default function LocationCard(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const {location} = props
  //console.log(location)

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const menu = useMemo(() => {
      //console.log(props.location)
      if(!props.location || !props.location.menu) return null;
      return props.location.menu.filter(meal => meal.Name === props.mealToShow)[0]
  }, [props.location, props.mealToShow])

  const [totalScore, filteredItems] = useMemo(() => {
    if(!menu) return [null, null];
    if(!props.favorites) return [0, {}];
    const scoredItems = {};
    let total = 0
    menu.Stations.forEach(station => {
        station.Items.forEach(item => {
            if(!scoredItems[item.Name]) {
                const score = window.localStorage.getItem(item.Name)
                //console.log(score)
                if(score !== null) {
                    total += parseInt(score);
                    scoredItems[item.Name] = true;
                }
            }
        })
    })
    return [total, scoredItems]
  }, [menu, props.favorites])

  const cardImage = useMemo(() => {
    if(!props.location || !props.location.ImageUrls || !props.location.ImageUrls[0]) return null;
    return <CardMedia
        className={classes.media}
        image={props.location.ImageUrls[0]}
        title={props.location.FormalName}
    />
  }, [props.location, classes.media]);

  if(!location) return null;
  if(!menu) return null;
  if(menu.Status !== 'Open') return null;
  const closeTime = format(parse(menu.Hours.EndTime, 'HH:mm:ss', new Date()), 'h:mm aaaa');

  return (
    <Card className={classes.card} style={{order:1000 - totalScore}}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe">
            {totalScore}
          </Avatar>
        }
        title={location.Name}
        subheader={`Closes ${closeTime}`}
        titleTypographyProps={{
          variant:'h6'
        }}
      />
      {cardImage}
      <Collapse in={!expanded} timeout="auto">
        <CardContent>
            <Typography variant="body2" color="textSecondary" component="p">
            {Object.keys(filteredItems).length > 0 ? Object.keys(filteredItems).join(', ') : 'No items to display'}
            </Typography>
        </CardContent>
      </Collapse>
      <CardActions disableSpacing>
        <Button
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })} />
          Show {expanded ? 'Filtered' : 'All'}
        </Button>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          {menu.Stations.map((station, i) => {
            return <Fragment key={station.Name}>
                <Typography paragraph>
                    {station.Name}
                </Typography>
                {station.Items.map(item => {
                    return <Typography key={station.Name + item.Name} variant="body2" color="textSecondary" component="p" onClick={() => {
                      let score = parseInt(window.localStorage.getItem(item.Name)) || 1;
                      props.editFavorite(item.Name, score)
                    }}>
                        {item.Name}
                    </Typography>
                })}
                {i === menu.Stations.length - 1 ? null : <hr/>}
            </Fragment>
          })}
        </CardContent>
      </Collapse>
    </Card>
  );
}