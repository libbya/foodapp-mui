const axios = require('axios');

const locations_url = '/api/locations';
const menu_url = (location, date) => {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    //console.log(`https://api.hfs.purdue.edu/menus/v2/locations/${location}/${year}-${month}-${day}`);
    return  `/api/menu/${location}/${year}-${month}-${day}`;
}

const getLocations = () => {
    const res = axios.get(locations_url);
    return res;
}

const getMenu = (location, date) => {
    return axios.get(menu_url(location, date))
}
module.exports.locations_url = locations_url;
module.exports.menu_url = menu_url;

module.exports.getLocations = getLocations;
module.exports.getMenu = getMenu;