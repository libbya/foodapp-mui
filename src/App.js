import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Nav from './components/Nav'
import AddFavorite from './components/AddFavorite';
import HomePage from './components/pages/HomePage';
import FavoritesPage from './components/pages/FavoritesPage';
import SettingsPage from './components/pages/SettingsPage';
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import Calendar from './components/Calendar';
import { getLocations, getMenu } from './scripts/api';

function App() {
  const [meal, setMeal] = useState('')
  const [locations, setLocations] = useState([]);
  const [hiddenTypes, setHiddenTypes] = useState(JSON.parse(window.localStorage.getItem('')) || {
    "On-the-GO!": true,
    "Quick Bites": true,
    "Dining Courts": true
  });
  const [showAddFavorite, setAddFavorite] = useState(false);
  const [showCalendar, setCalendar] = useState(false);
  const [selectedDate, handleDateChange] = useState(new Date());
  const [favoriteForm, setFavoriteForm] = useState({
    nameText: '',
    score: 1
  })
  useEffect(() => {
    const updateLocations = async () => {
      const res = await getLocations();
      //console.log(res)
      let locs = res.data.Location.filter(loc => hiddenTypes[loc.Type]);
      const menus = await Promise.all(locs.map(loc => getMenu(loc.Name, selectedDate)));
      locs = locs.map((loc, idx) => ({...loc, menu: menus[idx].data.Meals || null}));
      //console.log('setting new locations')
      //console.log(locs[0])
      setLocations(locs);
    }
    updateLocations();
  }, [selectedDate, hiddenTypes]);
  const [favorites, setFavorites] = useState(Object.entries(window.localStorage).filter(entry => entry[0]).map(entry => [entry[0], parseInt(entry[1])]))
  const editFavorite = (name, score) => {
    setFavoriteForm({
      nameText: name,
      score
    })
    setAddFavorite(true);
  }
  const toggleHiddenType = type => {
    setHiddenTypes({...hiddenTypes, [type]: !hiddenTypes[type]});
    window.localStorage.setItem('', JSON.stringify({...hiddenTypes, [type]: !hiddenTypes[type]}));
  }
  const toggleAllHiddenTypes = () => {
    let newHiddenTypes = {}
    if (Object.entries(hiddenTypes).some(([_, show]) => show)) {
      Object.keys(hiddenTypes).forEach(key => newHiddenTypes[key] = false);
    } else {
      Object.keys(hiddenTypes).forEach(key => newHiddenTypes[key] = true);
    }
    setHiddenTypes(newHiddenTypes);
    window.localStorage.setItem('', JSON.stringify(newHiddenTypes));
  }
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <HomePage
            locations={locations}
            favorites={favorites}
            mealToShow={meal}
            setMeal={setMeal}
            date={selectedDate}
            editFavorite={editFavorite}
          />
        </Route>
        <Route exact path="/favorites">
          <FavoritesPage
            favorites={favorites}
            editFavorite={editFavorite}
            removeFavorite={name => {
              console.log('Remove', name)
              window.localStorage.removeItem(name);
              setFavorites(favorites.filter(favorite => favorite[0] !== name));
            }}
          />
        </Route>
        <Route exact path="/settings">
            <SettingsPage
              hiddenTypes={hiddenTypes}
              toggleHiddenType={toggleHiddenType}
              toggleAllHiddenTypes={toggleAllHiddenTypes}
            />
        </Route>
      </Switch>
      <Nav setAddFavorite={setAddFavorite} setCalendar={setCalendar} selectedDate={selectedDate} setMeal={setMeal}/>
      <AddFavorite
        open={showAddFavorite}
        onClose={() => setAddFavorite(false)}
        {...favoriteForm}
        setForm={setFavoriteForm}
        addFavorite={() => {
          window.localStorage.setItem(favoriteForm.nameText, `${favoriteForm.score}`);
          let updated = false;
          setFavorites(favorites.map(favorite => {
            if(favorite[0] === favoriteForm.nameText) {
              updated = true;
              return [favorite[0], favoriteForm.score];
            } else {
              return favorite;
            }
          }))
          if(!updated) {
            setFavorites([...favorites, [favoriteForm.nameText, favoriteForm.score]])
          }
        }}
      />
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Calendar
          open={showCalendar}
          onClose={() => setCalendar(false)}
          value={selectedDate}
          onChange={handleDateChange}
        />
      </MuiPickersUtilsProvider>
    </Router>
  );
}

export default App;
